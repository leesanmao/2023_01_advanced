import useRouterInfo from './useRouterInfo'

export default function withRouter(Component) {
    return function HOC(props) {
        // 基于自定义Hook把常用的信息获取到，最后基于属性传递给组件
        const attrs = useRouterInfo()
        // 渲染真正的组件
        return <Component {...props} {...attrs} />
    }
}