import { useNavigate, useLocation, useParams, useSearchParams, useMatch } from 'react-router-dom'

export default function useRouterInfo() {
    const navigate = useNavigate(),
        location = useLocation(),
        params = useParams(),
        searchParams = useSearchParams()[0],
        match = useMatch(location.pathname)
    const attrs = {
        navigate,
        location,
        params,
        searchParams,
        match
    }
    return attrs
}