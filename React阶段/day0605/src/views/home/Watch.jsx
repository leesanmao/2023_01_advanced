import React from "react"
import { Button } from 'antd'
import { useNavigate, useLocation, useParams, useMatch, useSearchParams } from 'react-router-dom'
import qs from 'qs'

const Watch = function Watch(props) {
    const { navigate } = props
    /*
     navigate(to,options)
       to：跳转的地址/对象
       options：其余的配置项，例如「replace/state...」
     navigate(1)/navigate(-1) 前进后退
     navigate(N) 跳转到指定数字的历史记录
     */

    const handle = () => {
        // 这个是需要传递的信息
        let obj = {
            lx: 1,
            name: 'zhufeng'
        }
        let str = qs.stringify(obj)

        /* 
        // @1 问号传参
        // navigate(`/home/message?${str}`)
        navigate({
            pathname: `/home/message`,
            search: str  //必须是字符串格式
        }) 
        */

        /* // @2 隐式传参
        navigate('/home/message', {
            state: obj
        }) */

        // @3 路径参数
        navigate(`/home/message/1/zhufeng`)
    }

    return <div>
        控制面板 - 数据监控
        <Button type="primary" onClick={handle}>跳转</Button>
    </div>
}
export default Watch