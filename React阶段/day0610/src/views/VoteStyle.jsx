import styled from "styled-components"

const VoteStyle = styled.div`
    box-sizing: border-box;
    margin: 20px auto;
    padding:10px 20px;
    width: 300px;
    border: 1px solid #DDD;

    .title{
        display: flex;
        justify-content: space-between;
        align-items: center;
        line-height: 50px;
        font-size: 18px;
        border-bottom:1px dashed #DDD;

        span{
            color: #ff4d4f;
        }
    }

    .main-box{
        padding: 10px 0;

        p{
            font-size: 14px;
            line-height: 30px;
        }
    }
    
    .footer-box{
        .ant-btn{
            margin-right: 10px;
        }
    }
`
export default VoteStyle