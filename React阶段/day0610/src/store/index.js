import { legacy_createStore, applyMiddleware } from 'redux'
import reducer from './reducers'
import reduxLogger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import saga from './saga'

// 使用中间件
const env = process.env.NODE_ENV
const sagaMiddleware = createSagaMiddleware()
const middleware = [sagaMiddleware]
if (env !== 'production') middleware.push(reduxLogger)

const store = legacy_createStore(
    reducer,
    applyMiddleware(...middleware)
)
sagaMiddleware.run(saga) //启动SAGA的监听器
export default store