import { useState } from 'react'
import styled from 'styled-components'
import { Button, Tag, Table, Popconfirm } from 'antd'
import _ from '@/assets/utils'

/* 组件样式 */
const TaskStyle = styled.div`
    box-sizing: border-box;
    margin: 0 auto;
    width: 800px;

    .header-box{
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 10px 0;
        border-bottom: 1px solid #DDD;

        .title{
            font-size: 20px;
            font-weight: normal;
        }
    }

    .tag-box{
        margin: 15px 0;

        .ant-tag{
            padding: 5px 15px;
            margin-right: 15px;
            font-size: 14px;
            cursor: pointer;
        }
    }

    .ant-btn-link{
        padding: 4px 5px;
    }
`

/* 组件视图 */
// 定义表格列
const columns = [{
    title: '编号',
    dataIndex: 'id',
    width: '6%',
    align: 'center'
}, {
    title: '任务描述',
    dataIndex: 'task',
    width: '50%',
    ellipsis: true
}, {
    title: '状态',
    dataIndex: 'state',
    width: '10%',
    align: 'center',
    render: text => +text === 1 ? '未完成' : '已完成'
}, {
    title: '完成时间',
    width: '16%',
    align: 'center',
    render(text, record) {
        let { state, time, complete } = record
        time = +state === 1 ? time : complete
        return _.formatTime(time, '{1}/{2} {3}:{4}')
    }
}, {
    title: '操作',
    width: '18%',
    render(text, record) {
        let { state } = record
        return <>
            <Popconfirm title="您确定要删除此任务吗?" onConfirm={() => { }}>
                <Button type="link" danger>删除</Button>
            </Popconfirm>
            {+state === 1 ?
                <Popconfirm title="您确定要把此任务设置为已完成吗?" onConfirm={() => { }}>
                    <Button type="link">完成</Button>
                </Popconfirm> :
                null
            }
        </>
    }
}]
export default function Task() {
    // 定义状态
    let [data, setData] = useState([]),
        [loading, setLoading] = useState(false)

    return <TaskStyle>
        <div className="header-box">
            <h2 className="title">TASK OA 任务管理系统</h2>
            <Button type="primary">新增任务</Button>
        </div>

        <div className="tag-box">
            <Tag color="#1677ff">全部</Tag>
            <Tag>未完成</Tag>
            <Tag>已完成</Tag>
        </div>

        <Table columns={columns} dataSource={data} loading={loading} pagination={false} rowKey="id" size="middle" />
    </TaskStyle>
}