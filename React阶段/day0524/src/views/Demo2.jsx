import { useState, useEffect, useLayoutEffect } from 'react'

/* 
 useEffect VS useLayoutEffect
   useEffect：基于 useEffect 放在 effect链表 中的 callback 函数，会在组件渲染/更新完毕后触发执行「完毕指：已经把 virtualDOM 渲染为 真实的DOM，并且浏览器已经把真实DOM绘制到页面中」
   useLayoutEffect：会阻碍GUI线程的渲染，也就是在通知 effect链表 中的 callback 函数执行的时候，让浏览器停止对真实DOM的绘制，需要先把 callback 执行完，再去绘制！
 */
export default function Demo() {
    
    let [x, setX] = useState(0)

    useLayoutEffect(() => {
        if (x === 0) {
            setX(+new Date())
        }
    }, [x])

    return <div
        style={{ background: 'lightblue', userSelect: 'none' }}
        onClick={() => setX(0)}>
        {x}
    </div>
}