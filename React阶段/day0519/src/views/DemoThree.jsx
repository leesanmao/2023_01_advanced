/* 
非受控组件：不受状态的管控，直接操作DOM「不推荐的」
我们需要获取DOM元素 
  + 契机：只有等待组件第一次渲染完毕后，我们才可以获取到真实的DOM
  + 可以基于传统获取DOM的方式来操作
  + 在React/Vue框架中，都有提供好的获取DOM元素的方式 -> “ref”

@1 很旧的方案：给ref属性设置为一个字符串
  <div ref="AAA">...</div>
  this.refs.AAA -> DOM元素
  但是这种方式官方已经不推荐了，在React.StrictMode模式下，控制台会报错！

@2 给ref设置的属性值是一个函数「我喜欢的方案」
  <div ref={x => this.AAA = x}>
  this.AAA -> DOM元素

@3 基于官方推荐的 React.createRef（类组件/函数组件） / useRef（函数组件） 方法去处理
  基于 createRef/useRef 创建一个“ref对象”：{ current: null }
  让元素的ref属性值，等于创建的“ref对象”
  最后基于 ref对象.current 属性，获取真实的DOM

------------
ref除了可以获取视图中的真实DOM元素，还可以获取子组件的实例
  + ref绑定一个元素标签，目的是获取这个真实的DOM
  + ref绑定一个组件，目的是获取组件的实例（或者组件内部的某些元素，再或者是组件暴露出来的属性和方法）
    给类组件绑定ref，最后获取的的是类(子)组件的实例！子组件的实例都获取到了，那么在父组件中，再想获取子组件中的啥信息，直接基于实例处理即可！
    如果是给函数组件，绑定ref，则直接报错「因为是不被允许的」;但是我们是有办法解决的！！
*/
import React, { Component, createRef } from 'react'

/* class DetailBox extends Component {
    state = { x: 100 }
    detailBox = createRef()
    render() {
        return <div className="detail" ref={this.detailBox}>
            我是一个详细信息  {this.state.x}
        </div>
    }
} */

function DetailBox() {
    return <div className="detail">
        我是一个详细信息
    </div>
}

class DemoThree extends Component {
    render() {
        return <div className="box">
            <button onClick={() => {
                console.log(this.AAA)
            }}>开关</button>
            <DetailBox ref={x => this.AAA = x} />
        </div>
    }
}

/* 
class DemoThree extends Component {
    AAA = createRef()  // this.AAA = { current: null }
    render() {
        return <div className="box">
            <button onClick={() => {
                console.log(this.AAA.current)
            }}>开关</button>
            <br />
            <div className="detail" ref={this.AAA}>
                我是一个详细信息
            </div>
        </div>
    }
} 
*/

// 受控组件：由数据/状态来管控视图的渲染
/* 
class DemoThree extends Component {
    state = {
        show: true
    }
    render() {
        let { show } = this.state
        return <div className="box">
            <button onClick={() => {
                this.setState({
                    show: !show
                })
            }}>开关</button>
            <br />
            <div className="detail"
                style={{
                    display: show ? 'block' : 'none'
                }}>
                我是一个详细信息
            </div>
        </div>
    }
} 
*/
export default DemoThree