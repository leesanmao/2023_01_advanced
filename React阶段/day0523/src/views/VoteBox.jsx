import React, { useState } from "react"
import { Button } from 'antd'
import VoteBoxStyle from "./VoteBoxStyle"

export default function VoteBox() {
    let [supNum, setSupNum] = useState(() => {
        // 如果状态的初始值是需要经过复杂计算算出来的，而且只需要第一次使用，我们基于函数的方式处理
        // 此函数只对第一次执行 useState；并把返回值，作为状态的初始值！
        let ran = Math.round(Math.random() * 9 + 1)
        // ...
        return ran
    })
    let [oppNum, setOppNum] = useState(2)

    const handle = (type) => {
        if (type === 'sup') {
            setSupNum(supNum + 1)
            return
        }
        setOppNum(oppNum + 1)
    }

    return <VoteBoxStyle>
        <h2 className="title">
            React其实也不难!
            <span>{supNum + oppNum}</span>
        </h2>
        <div className="main-box">
            <p>支持人数：{supNum} 人</p>
            <p>反对人数：{oppNum} 人</p>
        </div>
        <div className="footer-box">
            <Button type="primary" onClick={handle.bind(null, 'sup')}>支持</Button>
            <Button type="primary" danger onClick={handle.bind(null, 'opp')}>反对</Button>
        </div>
    </VoteBoxStyle>
}


/* 
useState中多个状态的处理  
  官方推荐：需要多个状态，则执行多次 useState ，创建多个状态及修改状态的方法，让每个状态单独管理
  当然可以执行一次 useState，只不过创建的这个状态，需要是一个对象，对象中包含了需要的其它状态值
    但是这种方式我们不推荐
      + 基于 useState 获取的修改状态方法 setXxx ，在修改状态值的时候，不支持部分状态的更改「只有 React.Component.prototye.setState 才支持部分状态更改」
      + 你传递啥值，就把状态值整体改成啥「如何解决：在修改成为新的状态对象之前，先把现有的状态信息浅拷贝一份，在修改的时候，没改的用原始值，改变的地方用用新改的值」
        setXxx({
            ...state,
            supNum: state.supNum + 1
        })
*/
/* export default function VoteBox() {
    let [state, setState] = useState({
        supNum: 6,
        oppNum: 2
    })

    const handle = (type) => {
        if (type === 'sup') {
            setState({
                ...state,
                supNum: state.supNum + 1
            })
            return
        }
        setState({
            ...state,
            oppNum: state.oppNum + 1
        })
    }

    return <VoteBoxStyle>
        <h2 className="title">
            React其实也不难!
            <span>{state.supNum + state.oppNum}</span>
        </h2>
        <div className="main-box">
            <p>支持人数：{state.supNum} 人</p>
            <p>反对人数：{state.oppNum} 人</p>
        </div>
        <div className="footer-box">
            <Button type="primary" onClick={handle.bind(null, 'sup')}>支持</Button>
            <Button type="primary" danger onClick={handle.bind(null, 'opp')}>反对</Button>
        </div>
    </VoteBoxStyle>
} */

/* useState的基础知识  */
/* export default function VoteBox() {
    // 定义状态
    let [supNum, setSupNum] = useState(6),
        [oppNum, setOppNum] = useState(2)
    let total = supNum + oppNum,
        ratio = '- -'
    if (total > 0) ratio = (supNum / total * 100).toFixed(2) + '%'

    // 定义普通函数
    const handle = (type) => {
        if (type === 'sup') {
            setSupNum(supNum + 1)
            setTimeout(() => {
                console.log(supNum)
            }, 1000)
            return
        }
        setOppNum(oppNum + 1)
    }

    return <VoteBoxStyle>
        <h2 className="title">
            React其实也不难!
            <span>{total}</span>
        </h2>
        <div className="main-box">
            <p>支持人数：{supNum} 人</p>
            <p>反对人数：{oppNum} 人</p>
            <p>支持比率：{ratio}</p>
        </div>
        <div className="footer-box">
            <Button type="primary" onClick={handle.bind(null, 'sup')}>支持</Button>
            <Button type="primary" danger onClick={handle.bind(null, 'opp')}>反对</Button>
        </div>
    </VoteBoxStyle>
} */

/* 基于类组件完成需求 */
/* export default class VoteBox extends React.Component {
    state = {
        supNum: 6,
        oppNum: 2
    }

    handle = (type) => {
        let { supNum, oppNum } = this.state
        if (type === 'sup') {
            this.setState({
                supNum: supNum + 1
            })
            return
        }
        this.setState({
            oppNum: oppNum + 1
        })
    }

    render() {
        let { supNum, oppNum } = this.state,
            total = supNum + oppNum,
            ratio = '- -'
        if (total > 0) ratio = (supNum / total * 100).toFixed(2) + '%'

        return <VoteBoxStyle>
            <h2 className="title">
                React其实也不难!
                <span>{supNum + oppNum}</span>
            </h2>
            <div className="main-box">
                <p>支持人数：{supNum} 人</p>
                <p>反对人数：{oppNum} 人</p>
                <p>支持比率：{ratio}</p>
            </div>
            <div className="footer-box">
                <Button type="primary" onClick={this.handle.bind(null, 'sup')}>支持</Button>
                <Button type="primary" danger onClick={this.handle.bind(null, 'opp')}>反对</Button>
            </div>
        </VoteBoxStyle>
    }
} */