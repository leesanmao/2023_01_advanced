import { useState, useEffect } from 'react'
import { Button } from 'antd'
import { flushSync } from 'react-dom'

export default function Demo() {
    console.log('RENDER')
    let [x, setX] = useState(10),
        [y, setY] = useState(20),
        [z, setZ] = useState(30)

    const handle = () => {
        /* flushSync(() => {
            setX(x + 1)
        })
        console.log(x) //10
        setY(y + 1)
        setZ(z + 1)
        // 不论修改状态是同步还是异步，此处获取的 x 的值，永远都不会是最新修改的「用的都是现有闭包中的值」；最新修改的状态值，只能在下一个闭包中获取！！「即便把修改状态的操作，基于flushSync处理了，也仅仅是让其立即更新渲染一次，在它的下面，依然无法获取最新修改的状态值」
        // 而在类组件中，我们还可以基于 this.setState 中的 callback 函数，通过 this.state.xxx 获取最新状态值；或者基于 flushSync 把状态修改变为类似于同步效果，然后在 flushSync下面，就可以获取最新状态值！ */

        /* for (let i = 0; i < 10; i++) {
            setX(prev => {
                return prev + 1
            })
        } */

        setX(10)
    }

    return <div className="demo" style={{ padding: '50px' }}>
        <p>{x} - {y} - {z}</p>
        <Button type='primary' size='small' onClick={handle}>按钮</Button>
    </div>
}