import React from 'react'
import ReactDOM from 'react-dom/client'
/* ANTD */
import { ConfigProvider } from 'antd'
import zhCN from 'antd/locale/zh_CN'
import './index.less'
import Demo from './views/Demo2'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <ConfigProvider locale={zhCN}>
    <Demo />
  </ConfigProvider>
)