const { createProxyMiddleware } = require('http-proxy-middleware')
module.exports = function (app) {
    app.use(
        createProxyMiddleware("/api", {
            target: "https://news-at.zhihu.com/api/4",
            changeOrigin: true,
            pathRewrite: { "^/api": "" }
        })
    )
    app.use(
        createProxyMiddleware("/inst", {
            target: "https://www.jianshu.com/asimov",
            changeOrigin: true,
            pathRewrite: { "^/inst": "" }
        })
    )
}