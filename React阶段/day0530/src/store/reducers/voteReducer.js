import * as AT from '../action-types'

let initial = {
    title: 'React不是很难的!',
    supNum: 10,
    oppNum: 5
}
export default function voteReducer(state = initial, action) {
    state = { ...state }
    let { type } = action
    switch (type) {
        case AT.VOTE_SUP:
            state.supNum++
            break
        case AT.VOTE_OPP:
            state.oppNum++
            break
        default:
    }
    return state
}