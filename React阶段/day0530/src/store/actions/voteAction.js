import * as AT from '../action-types'
const voteAction = {
    support() {
        return {
            type: AT.VOTE_SUP
        }
    },
    oppose() {
        return {
            type: AT.VOTE_OPP
        }
    }
}
export default voteAction