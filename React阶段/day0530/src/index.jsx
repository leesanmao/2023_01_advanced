import React from 'react'
import ReactDOM from 'react-dom/client'
/* ANTD */
import { ConfigProvider } from 'antd'
import dayjs from 'dayjs'
import zhCN from 'antd/locale/zh_CN'
import 'dayjs/locale/zh-cn'
/* 组件&样式 */
import './index.less'
import Vote from './views/Vote'
/* STORE处理 */
import { Provider } from '@/myreactredux'
import store from './store'

dayjs.locale('zh-cn')

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <ConfigProvider locale={zhCN}>
    <Provider store={store}>
      <Vote />
    </Provider>
  </ConfigProvider>
)