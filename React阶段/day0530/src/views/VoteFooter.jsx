/* 
import React from "react"
import { Button } from 'antd'
import { useDispatch } from 'react-redux'
import action from "@/store/actions"

const VoteFooter = function VoteFooter() {
    const dispatch = useDispatch()
    return <div className="footer-box">
        <Button type="primary"
            onClick={() => {
                dispatch(action.vote.support())
            }}>
            支持
        </Button>
        <Button type="primary" danger
            onClick={() => {
                dispatch(action.vote.oppose())
            }}>
            反对
        </Button>
    </div>
}
export default VoteFooter
*/

import React from "react"
import { Button } from 'antd'
import { connect } from '@/myreactredux'
import action from "@/store/actions"

const VoteFooter = function VoteFooter(props) {
    let { support, oppose } = props
    return <div className="footer-box">
        <Button type="primary"
            onClick={support}>
            支持
        </Button>
        <Button type="primary" danger
            onClick={oppose}>
            反对
        </Button>
    </div>
}
export default connect(null, action.vote)(VoteFooter)


/*
 [mapDispatchToProps]：把我们需要派发的方法，基于属性传递给组件
   类型：函数、actionCreator对象、null/undefined
   dispatch => {
      // dispatch：store.dispatch 用来派发任务的方法
      return {
        // 返回对象中的信息，会基于属性传递给组件
        support(){
            return dispatch(
                action.vote.support()
                // {type: AT.VOTE_SUP}
            )
        },
        ...
      }
   }
   这样处理很麻烦，我们一般都是把其设置为 actionCreators 对象格式
 */

/* 
// connect 内部，自动基于 bindActionCreators 方法，把actionCreator对象，变为 mapDispatchToProps 这样的模式
// action.vote 「actionCreator对象：对象中包含好多方法，每个方法执行，都是返回需要派发的action对象」
{
     support() {
         return {
             type: AT.VOTE_SUP
         }
     },
     oppose() {
         return {
             type: AT.VOTE_OPP
         }
     }
} 
// 基于 bindActionCreators(action.vote,dispatch) 处理后
{
    support(){
        return dispatch(action.vote.support())
    },
    oppose(){
        return dispatch(action.vote.oppose())
    }
}
*/