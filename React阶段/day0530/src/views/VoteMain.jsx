import React from "react"
import { useSelector } from '@/myreactredux'

/*
 useSelector：react-redux提供的自定义Hook函数，只能在函数组件中运用
   + 获取基于 Provider 放在上下文中的 store 对象
   + 把让组件更新的办法自动加入到事件池中
   let xxx = useSelector(state=>{
      // 必须传递一个函数
      // state是容器中的总状态
      return { // 返回的对象中包含我们需要的状态信息，整体赋值给外面的 xxx
        a:state.vote.supNum,
        b:state.task.title,
        ...
      }
   })
   xxx.a -> state.vote.supNum
   xxx.b -> state.task.title

 useStore：把上下文中的 store 对象获取到
 */
const VoteMain = function VoteMain() {
    let { supNum, oppNum } = useSelector(state => state.vote)
    return <div className="main-box">
        <p>支持人数：{supNum} 人</p>
        <p>反对人数：{oppNum} 人</p>
    </div>
}
export default VoteMain
