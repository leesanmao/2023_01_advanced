import { useState, forwardRef, useImperativeHandle } from 'react'
import { Spin } from 'antd'
import styled from 'styled-components'
import PT from 'prop-types'

/* 组件样式 */
const LoadingStyle = styled.div.attrs(props => props)`
    position: fixed;
    top: 0;
    left: 0;
    z-index: 9999;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,.5);

    .content{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
    }

    .ant-spin,
    .text{
        display: block;
    }

    .text{
        line-height: 50px;
        font-size: ${props => {
        let { size } = props
        if (size === 'small') return 12
        if (size === 'middle') return 13
        return 14
    }}px;
        color: #1677ff92;
    }
`

const Loading = forwardRef(
    function Loading({ text, size }, ref) {
        let [show, setShow] = useState(true)
        const close = () => {
            setShow(false)
        }

        useImperativeHandle(ref, () => {
            return {
                show,
                close
            }
        })

        return <LoadingStyle
            size={size}
            style={{
                display: show ? 'block' : 'none'
            }}>
            <div className="content">
                <Spin size={size} />
                <span className="text">{text}</span>
            </div>
        </LoadingStyle>
    }
)
Loading.defaultProps = {
    text: '小主，奴家正在努力加载中...',
    size: 'large'
}
Loading.propTypes = {
    text: PT.string,
    size: PT.oneOf(['small', 'middle', 'large'])
}
export default Loading