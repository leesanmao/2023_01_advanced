import React from "react"
import styled from "styled-components"
import { NavLink, Outlet } from 'react-router-dom'

/* 组件样式 */
const HomeStyle = styled.div`
    padding: 10px;

    .nav-box{
        display: flex;
        border-bottom: 1px solid #EEE;

        a{
            padding: 0 20px;
            line-height: 40px;
            font-size: 15px;
            color: #000;

            &.active{
                color: #1677ff;
                font-size: 16px;
            }
        }
    }
`

const Home = function Home() {
    return <HomeStyle>
        <nav className="nav-box">
            <NavLink to="/home/watch">数据监控</NavLink>
            <NavLink to="/home/worker">工作台</NavLink>
            <NavLink to="/home/message">消息中心</NavLink>
        </nav>
        {/* 首页的三级路由 */}
        <Outlet />
    </HomeStyle>
}
export default Home