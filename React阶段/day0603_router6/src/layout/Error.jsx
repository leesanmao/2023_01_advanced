import React from "react"
import { Button, Empty } from "antd"
import styled from "styled-components"

/* 组件的样式 */
const ErrorStyle = styled.div`
    height: 100%;
    overflow: hidden;

    .ant-empty{
        position: relative;
        top: 100px;
    }
`

const Error = function Error() {
    return <ErrorStyle>
        <Empty description={<span>很遗憾，您访问的页面不存在！</span>}>
            <Button type="primary">返回首页</Button>
        </Empty>
    </ErrorStyle>
}
export default Error