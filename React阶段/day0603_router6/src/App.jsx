import React from "react"
import { HashRouter, Routes, Route, Navigate } from 'react-router-dom'

import BasicLayout from "./layout/BasicLayout"
import UserLayout from "./layout/UserLayout"
import Error from "./layout/Error"
import Login from "./views/Login"
import Register from "./views/Register"
import Home from "./views/Home"
import Personal from "./views/Personal"
import Category from "./views/Category"
import Watch from "./views/home/Watch"
import Worker from "./views/home/Worker"
import Message from "./views/home/Message"

const App = function App() {
    // 构建路由
    return <HashRouter>
        <Routes>
            <Route path="/" element={<BasicLayout />}>
                <Route path="" element={<Navigate to="/home" />} />
                <Route path="home" element={<Home />} >
                    <Route path="" element={<Navigate to="/home/watch" />} />
                    <Route path="watch" element={<Watch />} />
                    <Route path="worker" element={<Worker />} />
                    <Route path="message" element={<Message />} />
                </Route>
                <Route path="category" element={<Category />} />
                <Route path="personal" element={<Personal />} />
            </Route>
            <Route path="/user" element={<UserLayout />}>
                <Route path="" element={<Navigate to="/user/login" />} />
                <Route path="login" element={<Login />} />
                <Route path="register" element={<Register />} />
            </Route>
            <Route path="/404" element={<Error />} />
            <Route path="*" element={<Navigate to="/404" />} />
        </Routes>
    </HashRouter>
}
export default App