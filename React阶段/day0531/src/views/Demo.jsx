import React from "react"
import { Button } from "antd"
import styled from "styled-components"
import { connect } from 'react-redux'
import action from "@/store/actions"

// 组件样式
const DemoStyle = styled.div`
    box-sizing: border-box;
    margin: 20px auto;
    padding: 10px 20px;
    width: 200px;
    border: 1px solid #DDD;
    
    &>span{
        display: block;
        line-height: 35px;
        font-size: 18px;
    }
`

const Demo = function Demo(props) {
    let { num, increment } = props
    return <DemoStyle>
        <span>{num}</span>
        <Button type="primary"
            onClick={() => {
                increment(10)
            }}>
            新增
        </Button>
    </DemoStyle>
}
export default connect(
    state => state.demo,
    action.demo
)(Demo)