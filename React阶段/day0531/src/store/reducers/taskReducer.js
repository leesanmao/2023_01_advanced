import * as AT from '../action-types'

let initial = {
    list: null
}
export default function taskReducer(state = initial, action) {
    state = { ...state }
    let { type, list, id } = action
    switch (type) {
        case AT.TASK_QUERY_ALL_LIST:
            state.list = list
            break
        case AT.TASK_REMOVE:
            if (Array.isArray(state.list)) {
                state.list = state.list.filter(item => {
                    return +item.id !== +id
                })
            }
            break
        case AT.TASK_UPDATE:
            if (Array.isArray(state.list)) {
                state.list = state.list.map(item => {
                    if (+item.id === +id) {
                        item.state = 2
                        item.complete = new Date().toLocaleString('zh-cn', { hour12: false })
                    }
                    return item
                })
            }
            break
        default:
    }
    return state
}