import demoAction from "./demoAction"
import taskAction from "./taskAction"

const action = {
    demo: demoAction,
    task: taskAction
}
export default action