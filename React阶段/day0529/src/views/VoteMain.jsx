import React, { useContext } from "react"
import Theme from "@/Theme"
import useForceUpdate from "@/useForceUpdate"

const VoteMain = function VoteMain() {
    const { store } = useContext(Theme)

    let { supNum, oppNum } = store.getState().vote
    useForceUpdate(store)

    return <div className="main-box">
        <p>支持人数：{supNum} 人</p>
        <p>反对人数：{oppNum} 人</p>
    </div>
}

export default VoteMain