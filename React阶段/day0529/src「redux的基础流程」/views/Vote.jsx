import React, { useContext } from "react"
import VoteStyle from "./VoteStyle"
import VoteMain from "./VoteMain"
import VoteFooter from "./VoteFooter"
import Theme from "@/Theme"
import useForceUpdate from "@/useForceUpdate"

const Vote = function Vote() {
    const { store } = useContext(Theme)

    // 获取公共状态信息
    let { supNum, oppNum } = store.getState()
    useForceUpdate(store)

    return <VoteStyle>
        <h2 className="title">
            React其实也不难!
            <span>{supNum + oppNum}</span>
        </h2>
        <VoteMain />
        <VoteFooter />
    </VoteStyle>
}

export default Vote