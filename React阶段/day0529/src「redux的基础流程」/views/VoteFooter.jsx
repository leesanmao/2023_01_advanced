import React, { useContext } from "react"
import { Button } from 'antd'
import Theme from "@/Theme"

const VoteFooter = function VoteFooter() {
    const { store } = useContext(Theme)

    return <div className="footer-box">
        <Button type="primary"
            onClick={() => {
                store.dispatch({
                    type: 'sup'
                })
            }}>
            支持
        </Button>

        <Button type="primary" danger
            onClick={() => {
                store.dispatch({
                    type: 'opp'
                })
            }}>
            反对
        </Button>
    </div>
}

export default VoteFooter