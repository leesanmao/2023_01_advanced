import { createStore } from 'redux'

/* 
创建REDUCER管理员,修改容器中的公共状态 
  + state：容器中管理的公共状态信息
    在我们创建好store后，redux内部会默认派发一次(也就是把reducer执行一次)；而第一次执行reducer函数的时候，store容器中还没有公共状态信息呢，也就是此时的state=undefined；而我们写的 state = initial，就是为了在此时给容器赋值初始的状态信息！

  + action：派发的行为对象
    store.dispatch({
        //传递的这个对象就是action
        type:'xxx',
        ...
    })
    一定会通知reducer函数执行，dispatch中传递的对象，就是赋值给action形参的
      + action必须是一个对象
      + action对象中必须具备 type 属性「派发的行为标识」
    我们接下来就可以在reducer函数中，基于 action.type 的不同，修改不同的状态值！
  
  + 函数最后返回的信息，会整体替换store容器中的公共状态信息
*/
let initial = {
    supNum: 10,
    oppNum: 5
}
const reducer = function reducer(state = initial, action) {
    let { type } = action
    switch (type) {
        case 'sup':
            state.supNum++
            break
        case 'opp':
            state.oppNum++
            break
        default:
    }
    return state
}

/* 创建STORE容器,管理公共状态 */
const store = createStore(reducer)
export default store