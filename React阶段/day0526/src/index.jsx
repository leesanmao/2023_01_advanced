import React from 'react'
import ReactDOM from 'react-dom/client'
/* ANTD */
import { ConfigProvider } from 'antd'
import zhCN from 'antd/locale/zh_CN'
import dayjs from 'dayjs'
import 'dayjs/locale/zh-cn'
/* 组件&样式 */
import './index.less'
// import Task from './views/Task'
import Demo from './views/Demo2'

dayjs.locale('zh-cn')

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <ConfigProvider locale={zhCN}>
    {/* <Task /> */}
    <Demo />
  </ConfigProvider>
)