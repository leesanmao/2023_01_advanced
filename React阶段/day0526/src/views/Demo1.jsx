import { Button } from 'antd'
import { useState, useMemo } from 'react'
import styled from 'styled-components'

// 一点破样式
const DemoStyle = styled.div`
    box-sizing: border-box;
    margin: 20px auto;
    padding: 20px;
    width: 300px;
    border: 1px solid #DDD;

    p{
        font-size: 18px;
        line-height: 40px;
    }

    .ant-btn{
        margin-right: 10px;
    }
`

export default function Demo() {
    let [x, setX] = useState(10),
        [y, setY] = useState(20)

    // 假设：z的值是依赖于x状态值计算出来的「而且非常消耗性能」
    // useMemo就是在React中做优化的，类似于Vue中的computed计算属性；基于useMemo可以完成：
    //  + 组件第一次渲染，把useMemo中的callback执行，计算出一个值赋值给z「并且缓存这个值」
    //  + 以后组件的每一次更新，只有依赖的x状态发生改变，callback才会重新执行，计算出新的值；如果x状态没有变化，会使用之前缓存的值！
    // ===> 计算缓存：真实项目中，如果遇到 “依赖某个或者某几个状态，算出一个新的值” 这样的需求，并且计算的过程有一些复杂，需要消耗很多时间或者性能，此时我们基于 useMemo 对其进行优化处理！
    let z = useMemo(() => {
        console.time('AAA')
        let z = 0
        new Array(9999999).fill(null).forEach(() => {
            // ...
            z += x / 10
        })
        console.timeEnd('AAA')
        return z.toFixed(2)
    }, [x])

    return <DemoStyle>
        <p>{x} / {z} 和 {y}</p>
        <Button type="primary" size="small" onClick={() => setX(x + 1)}>修改X</Button>
        <Button type="primary" size="small" onClick={() => setY(y + 1)}>修改Y</Button>
    </DemoStyle>
}