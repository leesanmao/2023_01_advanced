// import http from "./http"

const delay = function delay(interval = 1000) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve()
        }, interval)
    })
}

/* 暴露API */
const API = {
    delay
}
export default API