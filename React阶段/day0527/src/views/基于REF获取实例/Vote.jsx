import { useRef, useEffect } from 'react'
import VoteStyle from "./VoteStyle"
import VoteMain from "./VoteMain"
import VoteFooter from "./VoteFooter"

export default function Vote() {
    let mainIns = useRef(),
        footerIns = useRef(),
        subBox = useRef()

    useEffect(() => {
        let { supNum, oppNum, change } = mainIns.current
        subBox.current.innerHTML = supNum + oppNum

        let { supBtn, oppBtn } = footerIns.current
        supBtn.current.addEventListener('click', change.bind(null, 'sup'))
        oppBtn.current.addEventListener('click', change.bind(null, 'opp'))
    }, [])

    return <VoteStyle>
        <h2 className="title">
            React其实也不难!
            <span ref={subBox}>0</span>
        </h2>
        <VoteMain ref={mainIns} />
        <VoteFooter ref={footerIns} />
    </VoteStyle>
}