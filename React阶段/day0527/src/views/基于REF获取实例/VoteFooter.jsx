import { Component, createRef } from 'react'
import { Button } from 'antd'

export default class VoteFooter extends Component {
    supBtn = createRef()
    oppBtn = createRef()

    render() {
        return <div className="footer-box">
            <Button type="primary" ref={this.supBtn}>支持</Button>
            <Button type="primary" danger ref={this.oppBtn}>反对</Button>
        </div>
    }
}