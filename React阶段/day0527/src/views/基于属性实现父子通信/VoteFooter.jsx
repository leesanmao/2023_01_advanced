import { PureComponent } from 'react'
import { Button } from 'antd'
import PT from 'prop-types'

export default class VoteFooter extends PureComponent {
    // 属性规则校验
    static propTypes = {
        change: PT.func.isRequired
    }

    render() {
        let { change } = this.props
        return <div className="footer-box">
            <Button type="primary" onClick={change.bind(null, 'sup')}>支持</Button>
            <Button type="primary" danger onClick={change.bind(null, 'opp')}>反对</Button>
        </div>
    }
}