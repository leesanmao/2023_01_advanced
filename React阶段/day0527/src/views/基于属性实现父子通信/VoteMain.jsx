import { memo, useMemo } from 'react'
import PT from 'prop-types'

const VoteMain = function VoteMain(props) {
    let { supNum, oppNum } = props
    let ratio = useMemo(() => {
        let result = '- -',
            total = supNum + oppNum
        if (total > 0) {
            result = (supNum / total * 100).toFixed(2) + '%'
        }
        return result
    }, [supNum, oppNum])

    return <div className="main-box">
        <p>支持人数：{supNum} 人</p>
        <p>反对人数：{oppNum} 人</p>
        <p>支持比率：{ratio}</p>
    </div>
}
// 属性的规则校验
VoteMain.defaultProps = {
    supNum: 0,
    oppNum: 0
}
VoteMain.propTypes = {
    supNum: PT.number,
    oppNum: PT.number
}

export default memo(VoteMain)