import API from '@/api'
import ButtonAgain from '@/components/ButtonAgain'

const Demo = function Demo() {
    const handle = async (ev) => {
        try {
            await API.delay(2000)
            // ...
        } catch (_) { }
    }

    return <div className="demo-box" style={{ margin: '20px auto', width: '200px' }}>
        <ButtonAgain type="primary" danger onClick={handle}>
            删除
        </ButtonAgain>

        <ButtonAgain type="primary" onClick={handle}>
            完成
        </ButtonAgain>

        <ButtonAgain
            onClick={(ev) => {
                console.log(ev)
            }}>
            哈哈哈
        </ButtonAgain>
    </div >
}

/* const Demo = function Demo() {
    let [loading, setLoading] = useState(false)

    const handle = async () => {
        setLoading(true)
        try {
            await API.delay(2000)
            // ...
        } catch (_) { }
        setLoading(false)
    }

    return <div className="demo-box" style={{ margin: '20px auto', width: '64px' }}>
        <Button type="primary" danger loading={loading} onClick={handle}>删除</Button>
    </div>
} */
export default Demo