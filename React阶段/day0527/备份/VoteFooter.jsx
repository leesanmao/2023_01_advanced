import { Component } from 'react'
import { Button } from 'antd'

export default class VoteFooter extends Component {
    render() {
        return <div className="footer-box">
            <Button type="primary">支持</Button>
            <Button type="primary" danger>反对</Button>
        </div>
    }
}