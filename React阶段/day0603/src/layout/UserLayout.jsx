import React from "react"
import { Tabs } from "antd"
import styled from "styled-components"
import backgroundImg from "@/assets/images/background.svg"
import logo from "@/assets/images/logo.svg"
import RouterView from "@/router"

/* 组件样式 */
const UserLayoutStyle = styled.div`
    position: relative;
    height: 100%;
    background: url(${backgroundImg}) no-repeat;
    background-size: 100%;

    .content{
        position: absolute;
        top: 100px;
        left: 50%;
        margin-left: -165px;
        width: 330px;
    }

    .header{
        .title{
            display: flex;
            justify-content: center;
            align-items: center;
            line-height: 44px;
            font-size: 33px;
            font-weight: normal;

            img{
                margin-right: 10px;
                width: 44px;
                height: 44px;
            }
        }
        
        .subtitle{
            margin: 12px 0 40px 0;
            text-align: center;
            font-size: 14px;
            color: rgba(0,0,0,.45);
        }
    }

    .ant-tabs{
        margin-bottom: 10px;

        .ant-tabs-nav{
            &:before{
                border-bottom-color:#DDD;
            }
        }

        .ant-tabs-tab{
            padding: 0 10px;
            font-size: 16px;
            line-height: 40px;
        }
    }
`

const UserLayout = function UserLayout() {
    // Tab页卡的数据
    const tabItem = [{
        label: `用户登录`,
        key: 'login',
    }, {
        label: `用户注册`,
        key: 'register',
    }]

    return <UserLayoutStyle>
        <div className="content">
            <div className="header">
                <h1 className="title">
                    <img src={logo} alt="" />
                    <span>Ant Design</span>
                </h1>
                <p className="subtitle">Ant Design 是西湖区最具影响力的 Web 设计规范</p>
            </div>
            <Tabs centered defaultActiveKey="login" items={tabItem} />
            {/* 登录/注册二级路由的位置 */}
            <RouterView name="user" />
        </div>
    </UserLayoutStyle>
}
export default UserLayout