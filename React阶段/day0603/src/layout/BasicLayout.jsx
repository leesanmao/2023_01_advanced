import styled from "styled-components"
import logo from "@/assets/images/logo.svg"
import avatar from "@/assets/images/touxiang.png"
import RouterView from "@/router"
import CommonMenu from "@/components/CommonMenu"

/* 组件样式 */
const BasicLayoutStyle = styled.div`
    height: 100%;
    overflow: hidden;

    .header{
        box-sizing: border-box;
        padding: 0 15px;
        height: 48px;
        background: #001529;
        display: flex;
        justify-content: space-between;
        align-items: center;

        .logo,
        .info{
            line-height: 48px;
            font-size: 18px;
            color: #FFF;
            display: flex;
            align-items: center;

            img{
                margin-right: 10px;
                width: 35px;
                height: 35px;
            }
        }

        .info{
            font-size: 14px;

            img{
                width: 30px;
                height: 30px;
            }
        }
    }

    &>.content{
        height: calc(100% - 48px);
        display: flex;

        .menu-box{
            height: 100%;
            background: #001529;

            .ant-btn{
                margin-left: 4px;
                background: transparent;
                box-shadow: none;
            }

            .ant-menu-item{
                padding: 0 24px;
            }

            .ant-menu-inline-collapsed{
                .ant-menu-item{
                    padding: 0 28px;
                }
            }
        }

        .view-box{
            box-sizing: border-box;
            padding: 15px;
            height: 100%;
            flex-grow: 1;

            .component{
                height: 100%;
                overflow-y: auto;
                overflow-x: hidden;
                background: #FFF;
            }
        }
    }
`

const BasicLayout = function BasicLayout(props) {
    return <BasicLayoutStyle>
        <div className="header">
            <h2 className="logo">
                <img src={logo} alt="" />
                Ant Design
            </h2>
            <div className="avatar">
                <p className="info">
                    <img src={avatar} alt="" />
                    海贼王-路飞
                </p>
            </div>
        </div>
        <div className="content">
            <CommonMenu />
            <div className="view-box">
                <div className="component">
                    {/* 主页的二级路由 */}
                    <RouterView name="basic" />
                </div>
            </div>
        </div>
    </BasicLayoutStyle>
}
export default BasicLayout