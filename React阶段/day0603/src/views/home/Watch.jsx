import React from "react"
import { Button } from 'antd'
import qs from 'qs'
import { useHistory } from 'react-router-dom'

const Watch = function Watch(props) {
    console.log(props)

    let history = useHistory()

    const handle = () => {
        // @1 问号传参
        /* let params = {
            lx: 1,
            name: 'zhufeng'
        }
        // history.push("/home/message?lx=1&name=zhufeng")
        history.push({
            pathname: '/home/message',
            // search: 'lx=1&name=zhufeng' //search必须是一个字符串类型的值
            search: qs.stringify(params)
        }) */

        // @2 隐式传参
        /* history.push({
            pathname: '/home/message',
            state: {
                lx: 1,
                name: 'zhufeng'
            }
        }) */

        // @3 路径参数
        // history.push(`/home/message`) //直接干到404
        // history.push(`/home/message/1`)
        // history.push(`/home/message/1/zhufeng`)
    }

    return <div>
        控制面板 - 数据监控
        <Button type="primary" onClick={handle}>跳转</Button>
    </div>
}
export default Watch