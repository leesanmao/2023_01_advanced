import React from "react"
import { Form, Input, Button } from "antd"
import { LockOutlined, PhoneOutlined } from '@ant-design/icons'
import styled from "styled-components"

/* 组件的样式 */
const RegisterStyle = styled.div`
    .submit{
        width: 100%;
    }

    .code-box{
        display: flex;
        justify-content: space-between;
        align-items: center;
        
        .ant-form-item{
            &:nth-child(1){
                margin-right: 10px;
            }
        }
    }
`

const Register = function Register() {
    return <RegisterStyle>
        <Form
            autoComplete="off"
            initialValues={{
                phone: '',
                code: ''
            }}>
            <Form.Item name="phone">
                <Input size="large" placeholder="请输入手机号" prefix={<PhoneOutlined />} />
            </Form.Item>
            <div className="code-box">
                <Form.Item name="code">
                    <Input size="large" placeholder="请输入验证码" prefix={<LockOutlined />} />
                </Form.Item>
                <Form.Item >
                    <Button size="large">发送验证码</Button>
                </Form.Item>
            </div>
            <Form.Item>
                <Button className="submit" type="primary" size="large">立即注册</Button>
            </Form.Item>
        </Form>
    </RegisterStyle>
}
export default Register