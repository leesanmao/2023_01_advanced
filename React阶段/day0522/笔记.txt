什么是事件？
  事件是浏览器赋予元素的默认行为，即便什么都不处理，元素该具备的事件都有！
  浏览器标准事件：https://developer.mozilla.org/zh-CN/docs/Web/Events#%E6%A0%87%E5%87%86%E4%BA%8B%E4%BB%B6

什么是事件绑定？
  给元素的事件行为绑定方法，当事件行为触发，会执行对应的方法，完成指定的需求！
  DOM0事件绑定：
    box.onclick = function(){ ... }
    原理：给元素对象的 onxxx 事件私有属性赋值
      + 只能给元素的此事件行为绑定一个方法
      + 必须拥有此事件私有属性才可以，某些标准事件是没有对应的事件私有属性的「例如：DOMContentLoaded」
  DOM2事件绑定：
    box.addEventListener('click',function(){ ... },布尔/对象)
      + 最后参数是布尔类型值，是控制触发的阶段  true:捕获  false:冒泡
      + 最后参数是对象，则是设置相关的配置项 
        + capture 控制触发阶段
        + once 只能触发一次，触发完毕后，会自动基于 removeEventListener 移除事件绑定
        + passive 设置为true之后，则禁止阻止默认行为
    原理：利用浏览器内置的事件池机制，完成事件绑定及触发管理的
      + 只要是浏览器支持的标准事件，都可以用 addEventListener 做事件绑定「比DOM0的功能强大」
      + 可以给同一个元素的同一个事件类型，绑定多个不同方法，当事件触发的时候，所有绑定的方法会依次被触发执行

什么是事件对象？
  所谓事件对象，就是一个对象，只不过记录了本次触发的相关信息；当事件触发，绑定的方法执行，会把分析好的事件对象，作为实参传递给每个函数！！
    box.addEventListener('click',function(ev){ 
       // ev：获取的事件对象信息
    })
  常用的事件对象：MouseEvent(PointerEvent/TouchEvent)、KeyboardEvent、Event...
  事件对象中常用的信息：
    altKey: false
    ctrlKey: false
    shiftKey: false
    clientX/clientY 操作点距离可视窗口的坐标
    pageX/pageY 操作点距离BODY的坐标
    srcElement/target 事件源
    type 事件类型
    which/keyCode 键盘的按键码
    ...
    preventDefault 阻止默认行为
    stopPropagation/stopImmediatePropagation 阻止冒泡传播
    composedPath 获取传播的路径「事件源 -> ... -> Window」
    ...

事件的传播机制
  对于支持事件传播的事件行为来讲，当事件行为触发的时候，会经历三个阶段：
    CAPTURING_PHASE: 1 捕获阶段「从window开始查找，一直找到事件源，其目的是为冒泡阶段分析好传播路径」
    AT_TARGET: 2 目标阶段「把事件源的相关事件行为触发」
    BUBBLING_PHASE: 3 冒泡阶段「并让其祖先元素的相关事件行为也被触发，而且是从事件源 -> Window」
  有一些事件行为是不支持“事件传播”的，例如：
    + mouseenter/mouseleave
    + load
    + ...

事件委托「事件代理」
  事件委托就是利用事件的传播机制，来实现的一种项目解决方案
  例如：一个容器中有很多后代元素，其中大部分后代元素，在点击的时候，都会处理一些事情「有些元素点击做相同的事情，有些点击做不同的事情」
    @1 传统方案：想操作哪些元素，就把这些元素全部都获取到，然后逐一做事件绑定
       这种方案不仅操作起来复杂，并且会开辟很多堆内存，性能也不是很好
    @2 新方案：利用事件的传播机制，不逐一获取元素和单独绑定事件了，而是只给外层容器做一个事件绑定，这样不管点击其后代中的哪一个元素，当前容器的点击事件也会触发，把绑定的方法执行；在方法中，我们只需要判断事件源是谁，从而处理不同的事情即可！ ===> 事件委托
       这种新方案的性能比传统方案提高40%以上！
  例如：我们容器中的元素不是写死的，而是动态绑定(动态创建)的，而且会持续动态创建；我们需要在点击每个元素的时候，做一些事情！
    @1 传统方案：每一次动态创建完，都需要获取最新的元素，给这些元素单独做事件绑定！
    @2 事件委托：只需要给容器的点击事件绑定方法，不论其内部元素是写死的，还是动态绑定的，只要点击了，都说明元素已经存在了，基于事件传播机制，我们只需要在外层容器中判断事件源，做不同的事情即可！
       事件委托可以给动态绑定的元素做事件绑定！！
  真实项目中还有很多需求，必须基于事件委托来完成！所以：以后再遇到事件绑定的需求，首先想是否有必要基于事件委托处理，如果确定有必要，则直接基于事件委托处理！

========================================
React中的合成事件 SyntheticEvent
  @1 什么是合成事件？
    合成事件是围绕浏览器原生事件，充当跨浏览器包装器的对象；它们将不同浏览器的行为合并为一个API，这样做是为了确保事件在不同浏览器中显示一致的属性！
    <div onXxx={} onXxxCapture={}> 这种在JSX元素上，基于onXxx绑定的事件，都是合成事件「通俗来讲，所谓合成事件，就是人家React内部对这些事件操作，进行过特殊的处理」

React18中的合成事件绑定原理「事件委托」
  @1 在JSX视图编译的时候，当遇到某个元素上出现 onXxx 合成事件绑定，其实并没有给元素做事件绑定，只是给这个元素设置了一个 onXxx 的私有属性，属性值就是我们绑定的方法！
     <div className="inner"
          onClick={() => {console.log('inner 冒泡「合成」')}}
          onClickCapture={() => {console.log('inner 捕获「合成」')}}>
     </div>
     渲染的结果是：
     inner.onClick = 函数
     inner.onClickCapture = 函数
     没有做任何的浏览器事件绑定，仅仅是给 inner 设置了两个私有属性！

  @2 在React内部，会给 #root 容器的点击行为，做事件绑定「捕获和冒泡阶段都处理了」；React所有的视图编译完毕后，都会插入到 #root 容器中，这样点击任何一个元素，都会把 #root 的点击行为触发！
     + 在React内部，默认会对大部分浏览器标准事件「前提：支持事件传播机制的」都进行了这样的委托处理！
     + 对于不支持事件传播的机制的事件，在JSX渲染的时候，就单独给元素做了相关的事件绑定！
     root.addEventListener('click',function(ev){
        // 获取传播路径
        // 按照从外到内的顺序，把元素的 onXxxCapture 合成事件的私有属性，依次触发执行
     },true)
     root.addEventListener('click',function(ev){
        // 获取传播路径
        // 按照从内到外的顺序，把元素的 onXxx 合成事件的私有属性，依次触发执行
     },false)

React16中，其合成事件的底层处理机制和React18有很大的区别：
  @1 首先绑定的合成事件，对于有传播机制的标准事件来讲，也不是做的事件绑定，而是变为元素的 onXxx/onXxxCapture 私有属性！！
  @2 只不过React16不是给 #root 做事件委托，而是委托给了 document，并且只做了“冒泡阶段”的委托
    document.addEventListener('click',function(){
      // 获取传播路径
      // 处理合成事件对象
      // 把 传播路径 按照倒序，依次执行 元素.onClickCapture() 方法
      // 把 传播路径 按照正序，依次执行 元素.onClick 方法
    },false)

React16中除了合成事件底层处理机制和18不同，对于合成事件对象的处理，和React18也是不一样的：
  在16版本中，存在事件对象池，用的合成事件对象只有一个，每一次事件触发，都是把合成事件对象的值，改为最新的值！但是一旦用完，则把这些值又释放掉了！
  在18版本中，取消了事件对象池机制，每一次事件触发都是创建新的合成事件对象，之前创建的合成事件对象，在没有用处的情况下，会被销毁掉！


========================================
在React项目中，我们在 .css/.less/.sass 等文件中编写的样式，默认都是“全局”样式；这样在组件合并渲染的时候，很可能导致各组件之间编写的样式发生冲突！所以我们期望：在各组件中写的样式，可以只对当前组件起作用，也就是组件样式的私有化处理方案！
  @1 样式私有化处理方案一：人为 有意识的/有规范的 规避冲突问题「常用方案」
    + 使用 less/sass/stylus 等预编译语言（主要是利用其提供的嵌套功能）
    + 制定组件命名、尤其是组件“根节点”样式的命名规范，保证所有组件“根节点”的样式类名是唯一的
      例如：组件所在的文件夹路径-组件名-box
      |-components
        |-TabBar.jsx  ->  common-tabbar-box
        |-...
      |-views
        |-home
          |-TableList.jsx  ->  home-table-list-box
          |-...
        |-device
          |-TableList.jsx  ->  device-table-list-box
          |-...
        |-Login.jsx   ->  login-box
        |-...
    + 而组件内部所有元素的样式，都要保证在该组件的样式类名下编写
      .device-table-list-box{
        .title{ 
          ... 
          .num{ ... }
        }
        span{ ... }
      }
    存在的弊端：
    + 如果选择器有过长的前缀，影响渲染的性能
    + 在样式表中编写的样式都是“静态样式”，无法基于JS等业务逻辑控制其“动态化”！

  @2 样式私有化处理方案二：CSS Modules
    这是一种基于技术手段，来保证编写样式类名的“唯一性”！
      + 样式表的命名：xxx.module.css「不再使用less/sass等预编译语言了」
      + 在 webpack 编译的时候，会把 .module.css 中所有的样式类都进行编译，变为唯一的样式类！
    例如：在Demo1.module.css文件中
      .demo {...}
      .title {...}
      .title span {...}
    经过编译后的样式
      .Demo1_demo__bLprq {...}
      .Demo1_title__EpCHF {...}
      .Demo1_title__EpCHF span {...}
    在组件中使用的时候
      import sty from './Demo1.module.css'
      /* sty = {
          demo: "Demo1_demo__bLprq",
          title: "Demo1_title__EpCHF"
      } */
      <div className={sty.demo}>
          <h2 className={sty.title}></h2>
      </div>

  @3 样式私有化处理方案三：基于技术手段，把CSS写在JS中，这样的处理思想，我们称之为“CSS IN JS”
    依赖第三方插件处理：React-JSS 或者 styled-components「常用」 ...
    安装 vscode 插件：vscode-styled-components
    使用步骤：
      + 导入插件，基于插件编写样式「相当于创建一个样式组件，其内部包含的是需要的样式」
        import styled from "styled-components"
        const colorRed = 'red'
        const DemoBox = styled.div`
            ...
            .title{
                ...,
                color: ${colorRed};
            }
        `
      + 视图中，直接基于创建的组件来渲染样式即可
        export default class Demo1 extends React.Component {
            render() {
                return <DemoBox>
                    <h2 className="title">...</h2>
                </DemoBox>
            }
        }
      编译的原理：在渲染 <DemoBox> 组件的时候，会被渲染为一个 div 标签，并给其设置一个 唯一的 样式类名！
        <div class="sc-bcXHqe bscqLV">
          <h2 class="title">...</h2>
        </div>
      而我们写的样式，最后也会编译为正常的CSS样式，不过都是在 “sc-bcXHqe bscqLV” 唯一的样式类下编写的！
        .bscqLV{...}
        .bscqLV .title{...}
        .bscqLV .title span{...}
    基于CSS-IN-JS这种思想处理样式：
      + 一方面基于技术手段保证了样式的私有化
      + 另一方面，因为CSS样式是写在JS中的，我们就可以根据业务逻辑，动态控制其渲染的样式
    实战开发中的细节问题：
      + 在多个“样式组件”嵌套的情况下，如果对相同元素都设置了样式，最后以谁的样式为主，就看优先级，如果想指定以谁为主，也是提高其优先级（!important）