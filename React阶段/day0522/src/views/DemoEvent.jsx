import React from "react"
import './DemoEvent.less'

export default class Demo extends React.Component {
    render() {
        return <div className="outer"
            onClick={(ev) => {
                console.log('outer 冒泡「合成」')
            }}
            onClickCapture={() => {
                console.log('outer 捕获「合成」')
            }}>

            <div className="inner"
                onClick={(ev) => {
                    // ev 合成事件对象
                    // ev.nativeEvent 原生事件对象

                    // 合成事件对象的阻止事件传播：阻止合成事件的传播以及原生事件的传播
                    // ev.stopPropagation() 

                    // 原生事件对象的阻止事件传播：只能阻止原生的事件传播
                    // ev.nativeEvent.stopImmediatePropagation()

                    console.log('inner 冒泡「合成」', ev)
                }}
                onClickCapture={() => {
                    console.log('inner 捕获「合成」')
                }}>
            </div>
        </div>
    }

    componentDidMount() {
        document.addEventListener('click', () => console.log('document 捕获'), true)
        document.addEventListener('click', () => console.log('document 冒泡'), false)

        document.body.addEventListener('click', () => console.log('body 捕获'), true)
        document.body.addEventListener('click', () => console.log('body 冒泡'), false)

        let root = document.querySelector('#root')
        root.addEventListener('click', () => console.log('root 捕获'), true)
        root.addEventListener('click', () => console.log('root 冒泡'), false)

        let outer = document.querySelector('.outer')
        outer.addEventListener('click', () => console.log('outer 捕获'), true)
        outer.addEventListener('click', () => console.log('outer 冒泡'), false)

        let inner = document.querySelector('.inner')
        inner.addEventListener('click', () => console.log('inner 捕获'), true)
        inner.addEventListener('click', (ev) => {
            // ev 原生事件对象
            // ev.stopImmediatePropagation()
            console.log('inner 冒泡')
        }, false)
    }
}