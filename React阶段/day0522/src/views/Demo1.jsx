import React from "react"
import styled from "styled-components"
import TitleBoxStyle from "@/assets/TitleBoxStyle"

// 编写样式
const DemoBox = styled.div`
    background: lightblue;

    .num{
        margin-left: 20px;
        color: darkred !important;
    }
`

export default class Demo1 extends React.Component {
    render() {
        return <DemoBox>
            <TitleBoxStyle size="large" color="#000">
                我是案例1
                <span className="num">10</span>
            </TitleBoxStyle>
        </DemoBox>
    }
}