/* 封装的一个通用样式组件 */
import styled from "styled-components"
const TitleBoxStyle = styled.h2.attrs(
    props => {
        // props: 接收调用样式组件，传递进来的属性
        // return 返回的信息，可以在下面的样式表中使用
        props = Object.assign({
            size: 'normal',
            color: '#555'
        }, props)
        return props
    }
)`
    font-size: ${props => {
        let size = props.size
        let result = ''
        switch (size) {
            case 'large':
                result = '18px'
                break
            case 'small':
                result = '14px'
                break
            default:
                result = '16px'
        }
        return result
    }};
    color: ${props => props.color};
    line-height: 40px;

    .num{
        color: darkgreen;
    }
`
export default TitleBoxStyle