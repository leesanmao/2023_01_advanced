import React from 'react'
import ReactDOM from 'react-dom/client'
import Demo2 from './views/Demo2'
import Demo1 from './views/Demo1'
/* ANTD */
import { ConfigProvider } from 'antd'
import zhCN from 'antd/locale/zh_CN'
import './index.less'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <ConfigProvider locale={zhCN}>
    <Demo1 />
    <Demo2 />
  </ConfigProvider>
)