import React from "react"
import styled from "styled-components"
import { Switch, Route, Redirect } from 'react-router-dom'
import Watch from "./home/Watch"
import Worker from "./home/Worker"
import Message from "./home/Message"

/* 组件样式 */
const HomeStyle = styled.div`
    padding: 10px;

    .nav-box{
        display: flex;
        border-bottom: 1px solid #EEE;

        a{
            padding: 0 20px;
            line-height: 40px;
            font-size: 15px;
            color: #000;
        }
    }
`

const Home = function Home() {
    return <HomeStyle>
        <nav className="nav-box">
            <a href="#/home/watch">数据监控</a>
            <a href="#/home/worker">工作台</a>
            <a href="#/home/message">消息中心</a>
        </nav>
        {/* 首页的三级路由 */}
        <Switch>
            <Redirect exact from='/home' to='/home/watch' />
            <Route path='/home/watch' component={Watch} />
            <Route path='/home/worker' component={Worker} />
            <Route path='/home/message' component={Message} />
            <Redirect to='/404' />
        </Switch>
    </HomeStyle>
}
export default Home