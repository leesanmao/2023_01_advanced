/* 
 柯理化函数「Curring，高阶函数」思想，其实就是闭包“保存”功能的一种运用
   @1 我们基于闭包，把一些值“事先存储”起来
   @2 这样其下级上下文，想用的时候，直接基于作用域链去获取即可
 */
/* 
const fn = function fn(...params) {
    // params：[1,2]
    return function (...args) {
        // args：[3]
        return params.concat(args).reduce((res, item) => res + item);
    };
};
let res = fn(1, 2)(3);
console.log(res); //=>6  1+2+3   
*/


const curring = function curring(...params) {
    const operate = (...args) => {
        // 把每一次函数执行传递的实参，都存储到params中
        params = params.concat(args);
        return operate;
    };
    // 在部分浏览器中，当我们基于 console.log 输出函数的时候，会自动调用函数的 toString 方法「即便不支持这个操作的浏览器，我们只要基于 “+函数” 操作，也可以调用函数的 toString 方法」
    operate.toString = () => {
        // 把函数之前每一次函数执行，传递进来的所有值，进行求和处理
        return params.reduce((res, item) => res + item);
    };
    return operate;
};
let res = curring(10)(1)(2)(3);
console.log(+res); //->16

res = curring(5)(1, 2, 3)(4);
console.log(+res); //->15

res = curring(10, 20)(1)(2)(3)(4)(5);
console.log(+res); //->45  
