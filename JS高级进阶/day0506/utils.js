/* 
// 和JQ学到的“如何处理各环境下运行”的思想
(function (global, factory) {
    "use strict";
    if (typeof module === "object" && typeof module.exports === "object") {
        // 在NodeJS或webpack环境中运行
        module.exports = factory(global, true);
    } else {
        // 在浏览器环境中运行
        factory(global);
    }
})(typeof window !== "undefined" ? window : this, function factory(window, noGlobal) {
    // 笼统检测是否为对象
    const isObject = function isObject(obj) {
        return obj !== null && /^(object|function)$/.test(typeof obj);
    };

    /!* 暴露API *!/
    let utils = {
        isObject
    };
    if (typeof noGlobal === "undefined") {
        window.utils = window._ = utils;
    }
    return utils;
}); 
*/

(function factory() {
    // 笼统检测是否为对象
    const isObject = function isObject(obj) {
        return obj !== null && /^(object|function)$/.test(typeof obj);
    };

    /* 暴露API */
    const utils = {
        isObject
    };
    if (typeof module === "object" && typeof module.exports === "object") {
        // 支持NodeJS和webpack环境
        module.exports = utils;
    } else if (typeof window !== "undefined") {
        // 支持浏览器环境
        window.utils = window._ = utils;
    }
})();


/*
// webpack
import _ from './utils'
_.isObject()

// node
const _ = require('./utils')
_.isObject()

// 浏览器
<script src='js/utils.js'></script>
<script>
_.isObject()
utils.isObject()
</script> 
*/