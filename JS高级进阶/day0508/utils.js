(function (global, factory) {
    "use strict"
    if (typeof module === "object" && typeof module.exports === "object") {
        module.exports = factory(global, true)
        return
    }
    factory(global)
})(
    typeof window !== "undefined" ? window : this,
    function factory(window, noGlobal) {
        /* 检测数据类型 */
        const toString = Object.prototype.toString,
            isArray = Array.isArray,
            typeReg = /^(object|function)$/,
            fnToString = Function.prototype.toString
        
        // 万能检测数据类型的方法
        const isType = function isType(obj) {
            if (obj == null) return obj + ''
            let type = typeof obj,
                reg = /^\[object (\w+)\]$/
            return !typeReg.test(type) ?
                type : //原始值类型
                reg.exec(toString.call(obj))[1].toLowerCase() //对象类型
        }

        // 检测是否为对象
        const isObject = function isObject(obj) {
            return obj !== null && typeReg.test(typeof obj)
        }

        // 检测是否是window对象
        const isWindow = function isWindow(obj) {
            return obj != null && obj === obj.window
        }

        // 检测是否为函数
        const isFunction = function isFunction(obj) {
            return typeof obj === "function"
        }

        // 检测是否为数组或者伪数组
        const isArrayLike = function isArrayLike(obj) {
            if (isArray(obj)) return true
            let length = !!obj && 'length' in obj && obj.length
            if (isFunction(obj) || isWindow(obj)) return false
            return length === 0 ||
                typeof length === "number" && length > 0 && (length - 1) in obj
        }

        // 检测是否为一个纯粹的对象(标准普通对象)
        const isPlainObject = function isPlainObject(obj) {
            if (isType(obj) !== "object") return false
            let proto, Ctor
            proto = Object.getPrototypeOf(obj)
            if (!proto) return true  //匹配 Object.create(null) 这种情况
            Ctor = proto.hasOwnProperty('constructor') && proto.constructor
            return isFunction(Ctor) && fnToString.call(Ctor) === fnToString.call(Object)
        }

        // 检测是否为空对象
        const isEmptyObject = function isEmptyObject(obj) {
            if (!isObject(obj)) throw new TypeError(`obj is not an object`)
            let keys = Reflect.ownKeys(obj)
            return keys.length === 0
        }

        // 检测是否为有效数字
        const isNumeric = function isNumeric(obj) {
            let type = isType(obj)
            return (type === "number" || type === "string") && !isNaN(+obj)
        }


        /* 其它基础方法 */

        // 迭代数组/伪数组/对象「支持中途结束循环」
        const each = function each(obj, callback) {
            if (typeof callback !== "function") callback = () => { }
            if (typeof obj === "number" && !isNaN(obj) && obj > 0) obj = new Array(obj).fill(null)
            if (typeof obj === "string") obj = Object(obj)
            if (!isObject(obj)) return obj
            if (isArrayLike(obj)) {
                for (let i = 0; i < obj.length; i++) {
                    let item = obj[i]
                    let res = callback.call(obj, item, i)
                    if (res === false) break
                }
                return obj
            }
            let keys = Reflect.ownKeys(obj)
            for (let i = 0; i < keys.length; i++) {
                let key = keys[i],
                    value = obj[key]
                let res = callback.call(obj, value, key)
                if (res === false) break
            }
            return obj
        }

        // 转移“_”的使用权
        let origin = null
        const noConflict = function noConflict() {
            if (window._ === utils) {
                window._ = origin
            }
            return utils
        }

        /* 暴露API */
        const utils = {
            isType,
            isObject,
            isArray,
            isArrayLike,
            isWindow,
            isFunction,
            isPlainObject,
            isEmptyObject,
            isNumeric,
            noConflict,
            each
        }
        if (typeof noGlobal === "undefined") {
            origin = window._
            window.utils = window._ = utils
        }
        return utils
    }
);