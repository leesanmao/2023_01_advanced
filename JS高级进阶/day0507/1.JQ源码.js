(function (global, factory) {
    "use strict";
    if (typeof module === "object" && typeof module.exports === "object") {
        module.exports = global.document ?
            factory(global, true) :
            function (w) {
                if (!w.document) {
                    throw new Error("jQuery requires a window with a document");
                }
                return factory(w);
            };
    } else {
        factory(global);
    }
})(
    typeof window !== "undefined" ? window : this,
    function factory(window, noGlobal) {
        // 我们在外面使用的JQ选择器 “ $('...') ”，其实就是把 jQuery 函数执行
        var jQuery = function (selector, context) {
            return new jQuery.fn.init(selector, context);
        };

        // JQ的原型对象
        jQuery.fn = jQuery.prototype = {
            constructor: jQuery,
            // ...
        };

        // init构造函数
        var rootjQuery = jQuery(document),
            init = jQuery.fn.init = function (selector, context, root) {
                // ...
            };
        init.prototype = jQuery.fn;
        

        /* 暴露API */
        if (typeof define === "function" && define.amd) {
            define("jquery", [], function () {
                return jQuery;
            });
        }
        if (typeof noGlobal === "undefined") {
            window.jQuery = window.$ = jQuery;
        }
        return jQuery;
    }
);